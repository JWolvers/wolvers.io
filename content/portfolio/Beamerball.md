---
title: "Beamerball"
description: "An inkball recreation, using a beamer and computer vision."
link: https://github.com/htr3n/laramod
screenshot: laramod.png
date: '2018-01-19'
layout: portfolio
---