---
title: "Foodjet Design Suite"
description: "A toolset to design printable food designes"
link: https://Foodjet.com
screenshot: laramod.png
date: '2019-11-29'
layout: portfolio
featured: true
---

[LaraMod](https://github.com/htr3n/laramod) is another modularisation effort to systematically organising a [Laravel](https://laravel.com) based project. The idea stems from my struggle to structure a Laravel-based project so that I can work effectively on individual modules whilst keeping Laravel codebase intact as much as possible and also keeping the project's codebase separate from Laravel.
