+++
title = "Hi there!"
date = "2019-12-02"
+++
I'm Jens Wolvers, an embedded system engineer in training, currently specializing in [Computer Vison](https://en.wikipedia.org/wiki/Computer_vision).
I study at the [HAN University of Applied Sciences](https://www.han.nl/international/english/) located in Arnhem, The Netherlands.

My Intrest in computer vision started at my work: [Foodjet](https://foodjet.com), a company specialized in food production machines. Where I recently help develop a machine that could cover any arbitrary shaped product in sauce (For example a pizza). This machine used computer vision to detect the products, and was the main reason I got intrested in Computer Vision.

Besides work I volenteer at a scouting club: [Scouting Weurt](https://scoutingweurt.nl/). Where I and other volenteers organize weekly activities for children. Teaching them about: Scouting techniques, sport and games, arts and expression, the outdoors, safty and health, society, identity, and international communication.

Some other things I am passionate about include: Motorsport, Simracing, Linux, Pop-music, my little car, but not cars in general. But first and foremost come my Family and Friends.

